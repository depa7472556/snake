// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

class ASnakeElement;
class ASnakePawn;

UENUM()
enum class EMovmentDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEPROJECT_API ASnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActor();
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElement>SnakeElementClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<ASnakeElement*>SnakeElements;

	UPROPERTY()
	EMovmentDirection LastMovmentDirection;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float MovementSpeed;
	
    UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum=1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElement*OverlappedBlock, AActor*Other);

	UFUNCTION(BlueprintCallable)
	void SnakeConsumeFood(int ElementsNum=1);

	UPROPERTY(EditAnywhere)
	bool IsMoving;
	
};
