// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakePawn.generated.h"

class UCameraComponent;
class ASnakeActor;
class AFood;


UCLASS()
class SNAKEPROJECT_API ASnakePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnakePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent*CameraComponent;

	UPROPERTY(BlueprintReadWrite)
    ASnakeActor*SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeActor>SnakeActorClass;

	void CreateSnakeActor();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void HandlePlayerVerticalInput (float NewValue);

	UFUNCTION()
	void HandlePlayerHorizenlInput (float NewValue);

	UPROPERTY(BlueprintReadWrite)
	AFood*FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodActorClass;

	void CreateFood();

private:
	float TickInterval;
};
