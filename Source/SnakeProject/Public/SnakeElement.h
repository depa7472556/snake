// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "SnakeElement.generated.h"

class UStaticMeshComponent;
class ASnakeActor;
UCLASS()
class SNAKEPROJECT_API ASnakeElement : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElement();
	ASnakeActor*SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent*MeshComponent;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();

	virtual void Interact(AActor* Interactor,bool BIsHead) override;

	UFUNCTION()
	void HandlBeginOverlap (UPrimitiveComponent*OverlappedComponent,
		                    AActor*OtherActor,
	                        UPrimitiveComponent*OtherComp,
		                    int32 OtherBodyIndex,
		                    bool bFromSweep,
		                    const FHitResult &SweepResult);


	UFUNCTION()
	void ToggleCollision();

	
};


