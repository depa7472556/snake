// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePawn.h"
#include "Food.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeActor.h"
#include "SnakeElement.h"
#include "Components/InputComponent.h"

// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    CameraComponent=CreateDefaultSubobject<UCameraComponent>(TEXT("CAmera"));
	RootComponent=CameraComponent;
	TickInterval = 1.f;
}

// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();
	CameraComponent->SetRelativeRotation(FRotator(-90,0,0));
    CreateSnakeActor();
	Cast<ASnakeActor>(SnakeActor);
	//SetActorTickInterval(TickInterval);

}  

void ASnakePawn::CreateSnakeActor()
{
	SnakeActor=GetWorld()->SpawnActor<ASnakeActor>(SnakeActorClass, FTransform());
}

void ASnakePawn::CreateFood()
{
	FVector FoodLocation (SnakeActor->SnakeElements[0]->GetActorLocation().X+FMath::RandRange(350,1000),
	                         SnakeActor->SnakeElements[0]->GetActorLocation().Y+FMath::RandRange(350,1000),
		                      SnakeActor->SnakeElements[0]->GetActorLocation().Z);
	FTransform FoodTransform (FoodLocation);
	FoodActor=GetWorld()->SpawnActor<AFood>(FoodActorClass, FoodTransform);
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	auto x = FMath::RandRange(1,100);
	if(x==5)CreateFood();

}

// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &ASnakePawn::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizon", this, &ASnakePawn::HandlePlayerHorizenlInput);
}

void ASnakePawn::HandlePlayerVerticalInput(float NewValue)
{
	if(IsValid(SnakeActor))
	{
		if(SnakeActor->IsMoving==true && NewValue>0 && SnakeActor->LastMovmentDirection!=EMovmentDirection::DOWN)
		{
			SnakeActor->IsMoving=false;
			SnakeActor->LastMovmentDirection=EMovmentDirection::UP;
			
		}
		else if (SnakeActor->IsMoving==true && NewValue<0 && SnakeActor->LastMovmentDirection!=EMovmentDirection::UP)
		{
			SnakeActor->IsMoving=false;
			SnakeActor->LastMovmentDirection=EMovmentDirection::DOWN;
		}
	}
}

void ASnakePawn::HandlePlayerHorizenlInput(float NewValue)
{
	if(IsValid(SnakeActor))
	{
		if(SnakeActor->IsMoving==true && NewValue>0 && SnakeActor->LastMovmentDirection!=EMovmentDirection::RIGHT)
		{
			SnakeActor->IsMoving=false;
			SnakeActor->LastMovmentDirection=EMovmentDirection::LEFT;
		}
		else if (SnakeActor->IsMoving==true && NewValue<0 && SnakeActor->LastMovmentDirection!=EMovmentDirection::LEFT)
		{
			SnakeActor->IsMoving=false;
			SnakeActor->LastMovmentDirection=EMovmentDirection::RIGHT;
		}
			
	}
}



