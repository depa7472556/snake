// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent=MeshComponent;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//Почему она в BP нормально работает от сюда нет?
	//FoodIsGood=FMath::RandBool();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor,bool BIsHead)
{
	if(BIsHead)
	{
		auto Snake = Cast<ASnakeActor>(Interactor);
		if(IsValid(Snake))
		{
			
			Snake->SnakeConsumeFood();
			if(FoodIsGood==true)
			{
				Snake->MovementSpeed-=0.02;
			}
			else
			{
				Snake->MovementSpeed+=0.01;	
			}
			
			Destroy();
		}
	}
}


