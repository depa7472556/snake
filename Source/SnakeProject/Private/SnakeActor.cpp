// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "SnakeElement.h"
#include "Interacteble.h"

// Sets default values
ASnakeActor::ASnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize =100.f;
	MovementSpeed =0.5f;
	LastMovmentDirection=EMovmentDirection::DOWN;
	IsMoving=true;
}
    
// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();
	IsMoving=true;
}  

void ASnakeActor::AddSnakeElement(int ElementsNum)
{
		for(int i = 0; i<ElementsNum;++i)
		{
			FVector NewLocation (SnakeElements.Num()*ElementSize,0,0);
			FTransform NewTrancform (NewLocation);
			ASnakeElement* SnakeElement =GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTrancform);
			SnakeElement->SnakeOwner=this;
			int ElemIndex = SnakeElements.Add(SnakeElement);
			if (ElemIndex==0)
			{
				SnakeElement->SetFirstElementType();
			}
		}
}
void ASnakeActor::Move()
{
	FVector MovementVector(ForceInitToZero);
	//float MovementSpeed = ElementSize;
	switch (LastMovmentDirection)
	{
	case EMovmentDirection::UP:
		MovementVector.X+=ElementSize;
		IsMoving=false;
		break;
	case EMovmentDirection::DOWN:
		MovementVector.X-=ElementSize;
		IsMoving=false;
		break;
	case EMovmentDirection::RIGHT:
		MovementVector.Y-=ElementSize;
		IsMoving=false;
		break;
	case EMovmentDirection::LEFT:
		MovementVector.Y+=ElementSize;
		IsMoving=false;
		break;
	}
	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
for(int i = SnakeElements.Num()-1; i>0; i--)
{
	auto CurrentElement = SnakeElements[i];
	auto PrevElement = SnakeElements[i-1];
	FVector PrevLocation = PrevElement->GetActorLocation();
	CurrentElement->SetActorLocation(PrevLocation);
}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeActor::SnakeElementOverlap(ASnakeElement* OverlappedBlock, AActor*Other)
{
	if(IsValid(OverlappedBlock))
	{
		int32 EleventIndex;
		SnakeElements.Find(OverlappedBlock, EleventIndex);
		bool bIsFirst = EleventIndex==0;
		IInteracteble*InteractebleInterface= Cast<IInteracteble>(Other);
		if(InteractebleInterface)
		{
			InteractebleInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeActor::SnakeConsumeFood(int ElementsNum)
{
	for(int i = 0; i<ElementsNum;++i)
	{
		FVector NewLocation (SnakeElements[1]->GetActorLocation());
		FTransform NewTrancform (NewLocation);
		ASnakeElement* SnakeElement =GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTrancform);
		SnakeElement->SnakeOwner=this;
		int ElemIndex = SnakeElements.Add(SnakeElement);
		if (ElemIndex==0)
		{
			SnakeElement->SetFirstElementType();
		}
	}
}




